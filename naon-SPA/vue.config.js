const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    outputDir: '../backend-typescript/www',
    configureWebpack: {
        entry: [
            "./src/app/main.js"
        ],
        plugins:[
        new CopyWebpackPlugin(
            [
              {
                from: './public',
                to: '../../backend-typescript/www',
                ignore: [
                  '.DS_Store',
                  'server.js',
                  'index.html'
                ]
              }
            ]
          )
        ]
    }
}