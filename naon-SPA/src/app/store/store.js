import Vue from 'vue';
import Vuex from 'vuex';
import axios from 'axios';

Vue.use(Vuex)

const AUTH_LOGIN = "AUTH_LOGIN";
const AUTH_REQUEST = "AUTH_REQUEST";
const AUTH_LOGOUT = "AUTH_LOGOUT";
const AUTH_SUCCESS = "AUTH_SUCCESS";
const AUTH_ERROR = "AUTH_ERROR";

const USER_REQUEST = "USER_REQUEST";


export default new Vuex.Store({
  state: {
    token: "",
    status: "",
  },
  mutations: {
    [AUTH_REQUEST]: (state) => {
      state.status = 'Signing In...'
    },
    [AUTH_SUCCESS]: (state, token) => {
      state.status = 'success'
      state.token = token
    },
    [AUTH_ERROR]: (state) => {
      state.status = 'Invalid Username or Password'
    },
    [AUTH_LOGOUT]:(state)=>{
      state.token = "";
    }
  },
  getters: {
    getToken: (state) => {
      return state.token;
    },
    isAuthenticated: (state) => {
      state.token = localStorage.getItem("user-token");
      axios.defaults.headers.common["authorised-token"] = state.token;
      if (state.token)
        return true;
      else
        return false;
    },
  },
  actions: {
    [AUTH_REQUEST]: ({commit, dispatch}, user)=> {
      return new Promise((resolve, reject)=> {
        commit(AUTH_REQUEST);
        axios({url: "/signin", data: user, method: "POST"})
          .then(resp => {
            const token = resp.data.authorisedToken;
            localStorage.setItem("user-token", token);
            axios.defaults.headers.common["authorised-token"] = token;
            commit(AUTH_SUCCESS, token);
            resolve(resp);
          })
          .catch(err => {
            commit(AUTH_ERROR, err)
            localStorage.removeItem('user-token') // if the request fails, remove any possible user token if possible
            reject(err)
          })
      })
    },
    [AUTH_LOGOUT]: ({commit, dispatch}) => {
      return new Promise((resolve, reject) => {
          commit(AUTH_LOGOUT)
          localStorage.removeItem('user-token')
          // remove the axios default header
          axios.defaults.headers.common['Authorization']=""
          resolve()
      })
  }

    /*createToken: (context, payload) => {
      context.commit("storeToken",payload);
    }*/

  }
})
