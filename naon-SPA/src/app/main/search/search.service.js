import axios from 'axios'
export default function search(searchTerm){
    return axios.post("/post/search",{searchTerm}).then(response => {return response.data});

}