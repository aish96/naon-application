import Vue       from "vue";
import VueRouter from "vue-router";
import axios from 'axios';
import store from './store/store';
import App from './App.vue';
import dotenv from 'dotenv';
import {routes} from "./main.routes";
import io from 'socket.io-client';


const postShell = () => import (/*webpackChunkName: postShell*/ "./main/shared/components/post-shell.vue");
const postForm = () => import (/*webpackChunkName: postForm*/ "./main/shared/components/post-form.vue");
const block = () => import(/*webpackChunkName: block*/ "./main/shared/components/block.vue");
const token = localStorage.getItem('user-token');

Vue.use(VueRouter);

Vue.component("candorism-post", postShell);
Vue.component("candorism-post-form", postForm);
Vue.component("candorism-block", block);

dotenv.config();

Vue.config.productionTip = false;

const endPoint = process.env.VUE_APP_NAON_ENDPOINT;
const apikey = process.env.VUE_APP_API_KEY;

console.log(`VUE_APP_NAON_ENDPOINT: ${ endPoint }`);
console.log(`VUE_APP_API_KEY: ${ apikey }`);

const socket = io(endPoint);

axios.defaults.baseURL=endPoint;

axios.interceptors.request.use(config => {
  const token = localStorage.getItem('user-token');
  
  if (token)
    config.headers['authorised-token'] = token;
  else
  config.headers['authorised-token'] = "";

  return config;

})

export const bus = new Vue();

const router = new VueRouter({
   
    routes
});

new Vue({
    router,
    store,
    render: h =>h(App)
}).$mount('#app');