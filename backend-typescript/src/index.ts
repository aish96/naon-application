import * as mongoose from 'mongoose';
import * as express from 'express';
import * as winston from 'winston';
import * as dotenv from 'dotenv';
import * as socketio from 'socket.io';

import {ErrorMiddleware}           from './middlewares/errorMiddleware';
import { ApiStartup }   from './startup/apiStartup';
import { SocketIOStartup }   from './startup/socketIOstartup';


//const swaggerDocument = require("./swagger.json");
const app = express();
const router = express.Router();

dotenv.config();

const portNumber = process.env.PORT || 3031;

winston.configure({
    level: "error",
    transports: [
        new winston.transports.File({
            filename: "logfile.log"
        })
    ]
})


process.on("uncaughtException", (err:Error)=>{
    winston.error(err);
    process.exit(1);
});

process.on("unhandledRejection",(err:Error)=>{
    winston.error(err);
    process.exit(1);
})


console.log(`DB Setting: ${process.env.DB_CONNECTION_STRING}`);


mongoose.connect(process.env.DB_CONNECTION_STRING,{useNewUrlParser: true}).then(()=> {
          console.log("DB Connected");
          startServer();
}).catch(err => {
    console.error("unable to connect to database " + err);
});


//app.use('/api-docs',swaggerUi.serve, swaggerUi.setup(swaggerDocument));

const startServer = () => {
    let http = require('http').Server(app);
    let io = require('socket.io')(http);
   
    const startExpress:ApiStartup = new ApiStartup(app,router,new ErrorMiddleware());
    const startSocket:SocketIOStartup = new SocketIOStartup(io);


    startExpress.start();
    startSocket.startSocket();
   

    http.listen(portNumber,'',() => {
        console.log(`Listening on ${portNumber}`);
    });
};

//startServer();