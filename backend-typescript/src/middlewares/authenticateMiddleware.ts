import * as express     from 'express';
import { asyncErrHandler } from "./asyncErrorHandler";
import * as jsonwebtoken   from 'jsonwebtoken';
import { json } from 'body-parser';

export const authenticateMiddleware = async(req: express.Request, res: express.Response, next: express.NextFunction)=>{
    
    let token:string = <string>req.headers["authorised-token"];
    if (!token){
        res.status(401).send("unauthorised access")
        return;
    }

    res.locals.userObj = jsonwebtoken.verify(token, process.env.SECRET);
    next();
};