import * as express  from 'express';

import {asyncErrHandler}   from '../../middlewares/asyncErrorHandler';
import { ReplyServiceCRUD }       from '../../services/replyServices/replyServiceCRUD';
import { ReplyServiceQuery }       from '../../services/replyServices/replyServiceQuery';
import { PostQuery }               from '../../services/postServices/postQuery';

import { IReplyDomain }                  from '../../models/replies';


export class ReplyRoute {
    constructor(
        private _replyCrud: ReplyServiceCRUD,
        private _replyQuery: ReplyServiceQuery,
        private _route: express.Router
    ) {
        this.setupRoute();
    }

    private setupRoute():void {
        this._route.get("/:postid/:pagenumber",  asyncErrHandler(async (req:express.Request, res:express.Response)=> {

            let postId = req.params.postid;
            let pagenumber = req.params.pagenumber;

            let replies = await this._replyQuery.GetReplyByPostId(postId, pagenumber,res.locals.userObj.username);
            res.send(replies);
         
        }));
        
        this._route.get("/:replyid/", asyncErrHandler(async(req:express.Request, res:express.Response)=> {
        
            let replyId = req.params.replyId;

            let reply = await this._replyQuery.GetReply(replyId);
            
            if (reply)
                res.send(reply);
            else
                res.sendStatus(404);
                
         
        
        }));
        
        this._route.get("/childreplies/:commentId/:pagenumber", asyncErrHandler(async(req:express.Request, res:express.Response)=>{
          
                let commentId = req.params.commentId;
                let pageNumber = req.params.pageNumber;

                let childReplies = await this._replyQuery.GetChildReplies(commentId, pageNumber ,res.locals.userObj.username);
        
                if (childReplies.length > 0)
                    res.send(childReplies);
                else res.sendStatus(400);
                
           
        }));
        
        this._route.post("/", asyncErrHandler(async(req:express.Request,res:express.Response)=>{
        
             
                let replyModel:IReplyDomain = {
                    username: res.locals.userObj.username,
                     post_id: req.body.post_id,
                     parent_reply_id: req.body.parent_reply_id,
                     reply_to_id: req.body.reply_to_id,
                     message: req.body.message,
                     reply_id: req.body.reply_id
                };
        
                if (!replyModel.reply_id || replyModel.reply_id.length == 0)
                    res.send(await this._replyCrud.addReply(replyModel));
                else
                    res.send(await this._replyCrud.editReply(replyModel));
        
          
        }));
    }

    public getRoute():express.Router {
        return this._route;
    }
}