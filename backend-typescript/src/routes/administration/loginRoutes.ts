import * as express from 'express';
import { asyncErrHandler } from "../../middlewares/asyncErrorHandler";
import {SignInService}   from '../../services/administrationService/signInService';

import { UserServiceQuery }     from '../../services/userServices/userServiceQuery';
import { UserServiceCRUD}       from '../../services/userServices/userServiceCRUD';


export class LoginRoutes{
  constructor(
    private _signInService: SignInService,
    private _route: express.Router
  ){
    this.setupRoute();
  }

  private setupRoute():void {
    this._route.post("/", asyncErrHandler(async (req:express.Request, res:express.Response)=> {
      
      let username: string = req.body.username;
      let password: string = req.body.password;

      let authObject: any = await this._signInService.verifyUser(username, password)
      if (authObject.result)
         res.send({authorisedToken: this._signInService.generateWebToken(authObject.userObject)});
       else
         res.status(401).send("Invalid credentials");      
    }))

    this._route.post("/register", asyncErrHandler(async (req:express.Request, res:express.Response)=> {
      let user = {
        username: req.body.username,
        password:req.body.password,
        firstname: req.body.firstname,
        middlename:req.body.middlename,
        lastname: req.body.lastname,
        address1: req.body.address1,
        address2: req.body.address2,
        suburb: req.body.suburb,
        postcode: req.body.postcode,
    }

   
        let userCrud = new UserServiceCRUD();
        let userQuery = new UserServiceQuery();

        let existingUser = await userQuery.getUserByUsername(user.username);

        if (existingUser){
            res.status(400).send("username already exists.  Please choose another one");
            return;
        }
        
        let result = await userCrud.registerUser(user);
        let signIn:SignInService = new SignInService();
        let authObject: any  = await signIn.verifyUser(user.username, user.password);
        
        res.send({authorisedToken: signIn.generateWebToken(authObject.userObject)});    
    }))
    
  }

  public getRoute():express.Router {
    return this._route;
  }
}