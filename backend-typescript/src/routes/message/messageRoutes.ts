import * as express from 'express';

import { asyncErrHandler } from "../../middlewares/asyncErrorHandler";

import { MessageCrud }   from '../../services/messageServices/messageCrud';
import { MessageQuery }   from '../../services/messageServices/messageQuery';

import { IMessageDomain, IMessage }                  from '../../models/messages';

export class MessageRoutes {
    constructor(
        private _messageCrud: MessageCrud,
        private _messageQuery: MessageQuery,
        private _route: express.Router
    ){
        this.setupRoute();
    }


    public getRoute(): express.Router {
        return this._route;
    }

    private setupRoute():void {
        this._route.get("/inbox/:pagenumber", asyncErrHandler(async(req:express.Request,res:express.Response)=>{
            let result = await this._messageQuery.getMessages(res.locals.username, req.params.pagenumber);

            res.send(result);
        }));

        this._route.post("/send", asyncErrHandler(async(req:express.Request,res:express.Response)=>{

            let message:IMessageDomain = {
                from: res.locals.username, 
                to: req.body.to, 
                message: req.body.message
            };
            let result:IMessage = await this._messageCrud.insertMessage(message);

            res.send(result);
        }))

        this._route.post("/markasread/:id", asyncErrHandler(async(req: express.Request, res: express.Response)=>{
            
            let result = await this._messageCrud.markAsRead(req.params.id);

            res.send(result);
        }))

        this._route.post("/markallasread", asyncErrHandler(async (req: express.Request, res: express.Response)=>{
            this._messageCrud.markAllAsRead(res.locals.username);
        }))
    }


}