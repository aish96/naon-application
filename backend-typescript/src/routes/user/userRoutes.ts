import * as express from 'express';
import { asyncErrHandler }   from    '../../middlewares/asyncErrorHandler';
import {Request, Response} from 'express';


import { UserServiceQuery }     from '../../services/userServices/userServiceQuery';
import { UserServiceCRUD}       from '../../services/userServices/userServiceCRUD';


export class UserRoute {
    constructor(
        private _userQuery: UserServiceQuery,
        private _userCrud: UserServiceCRUD,
        private _route: express.Router
    ){
        this.setupRoute();
    }

    private setupRoute():void {

        this._route.get("/", asyncErrHandler(async(req:Request, res:Response) => {

         
            let userService = new UserServiceQuery();
            let result = await userService.getUserByUsername(res.locals.userObj.username)
        
            if (result)
                res.send(result);
            else
                res.status(404).send("not found");
          
        }));
        
        this._route.get("/userdetails", asyncErrHandler(async(req:Request,res:Response) => {
        
         
                let userQuery = new UserServiceQuery();
        
                let result = await userQuery.getUserDetailsByUsername(res.locals.userObj.username);
        
                if (result)
                    res.json(result);
                else
                    res.status(404).send("User not found");
           
        
        }));

    }

    public getRoute():express.Router {
        return this._route;
    }
}


