export class SocketIOStartup{

        private _socket: any;

       constructor(
        private _io: any,
       ){}

       public startSocket():void {
         this._io.on("connection", (socket:any)=> {
            this._socket = socket;
            console.log(this._socket.client.id);

            socket.on("message", this.messageEvent)

         })
       }

       private messageEvent(message: any):void {
            
       }
}