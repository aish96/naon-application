import { MessageModel, IMessage }       from '../../models/messages';
import { UserServiceQuery }       from '../userServices/userServiceQuery';

export class MessageQuery {

    private _userQuery: UserServiceQuery = null;

    constructor(){
        this._userQuery = new UserServiceQuery();
    }

    public async getMessages(username: string, pageNumber: number){
        
        let limit:number = 5;

        let messages = await MessageModel.find({to: username}).limit(limit).skip((pageNumber - 1) * limit);

        return await Promise.all(messages.map(m => this.mapToResult));
    }

    private async mapToResult(message: IMessage) {
        let user = await this._userQuery.getUserDetailsByUsername(message.to);

        return {
            message: message.message,
            from: {
                username: message.from
            },
            dateSent: message.date
        }
    }
}