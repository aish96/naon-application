import { PostModel }   from '../../models/posts';

export class ImageQuery {
    public async GetImage(postId: string):Promise<{imageFile: Buffer, description: string, extension: string}>{
        let image = (await PostModel.findById(postId).select('image -_id')).image;
        return image;
    }
}