import * as mongoose from 'mongoose';
import { 
    Schema, 
    Model, 
    model, 
    Document 
}   from "mongoose";


export interface IReply extends Document {
    post_id: Schema.Types.ObjectId,
    message: String,
    parent_reply_id: Schema.Types.ObjectId,
    date: Date,
    username: String,
    modifiedDate: Date
}

export interface IReplyDomain {
    reply_id: string,
    post_id: string,
    message: string,
    parent_reply_id: string,
    reply_to_id: string,
    username: string,
}

let replySchema = new  mongoose.Schema({
    post_id: { 
        type: Schema.Types.ObjectId, 
        required: true
    },
    message: Schema.Types.String,
    parent_reply_id: {
        type: Schema.Types.ObjectId,
        required: false, 
    },
    createdate: {
        type: Schema.Types.Date,
        default: Date.now
    },
    username: Schema.Types.String,
    modifiedDate: {
        type:Schema.Types.Date,
        default: Date.now
    },

})

export const ReplyModel:Model<IReply> = model<IReply>("replies",replySchema);