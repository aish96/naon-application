import { 
    Schema, 
    Model, 
    model, 
    Document 
}   from "mongoose";

export interface ILike extends Document{
   postId: string,  //if likes for post
   replyId: string, //if likes for reply
   username: string, // who gives like
}

let likeSchema = new Schema({
    username: String,
    postId: String,
    replyId: String
})

export const LikeModel: Model<ILike> = model<ILike>("Likes",likeSchema);