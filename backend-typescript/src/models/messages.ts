import { 
    Schema, 
    Model, 
    model, 
    Document 
}   from "mongoose";


export interface IMessage extends Document{
    from: string,
    to: string,
    message: string,
    date: Date,
    isRead: boolean
}

export interface IMessageDomain {
    from: string,
    to: string,
    message: string,
}

let messageSchema = new Schema({
    from: String,
    to: String,
    message: String,
    date: {
        required: true,
        default: Date.now,
        type: Schema.Types.Date
    },
    isRead:{
        required: true,
        default: false,
        type: Schema.Types.Boolean
    }
});

export const MessageModel: Model<IMessage> = model<IMessage>("Messages",messageSchema);