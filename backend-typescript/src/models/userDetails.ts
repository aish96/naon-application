import * as mongoose from 'mongoose';
import { 
    Schema, 
    Model, 
    model, 
    Document 
}   from "mongoose";

export interface IUserDetails extends Document {
    username: string,
    firstname: string,
    middlename: string,
    lastname: string,
    photoProfile: Buffer,
    address1: string,
    address2: string,
    suburb: string,
    postcode: string,
    createDate: Date
}

export interface IUserDetailsDomain {
    username: string,
    firstname: string,
    middlename: string,
    lastname: string,
    photoProfile: Buffer,
    address1: string,
    address2: string,
    suburb: string,
    postcode: string,
}

const userDetailsSchema = new mongoose.Schema({
    username: String,
    firstname: String,
    middlename: {
        type: String,
        required: false,
    },
    lastname: String,
    photoprofile: Buffer,
    address1: String,
    address2: String,
    suburb: String,
    postcode: String,
    createDate: {
        type: Date,
        default: Date.now,
    }
});

export const UserDetailsModel: Model<IUserDetails> =  model<IUserDetails>("userDetails",userDetailsSchema);