import * as mongoose from 'mongoose';
import { 
    Schema, 
    Model, 
    model, 
    Document 
}   from "mongoose";

export interface IPost extends Document {
    username: string,
    message: string,
    dateTime: Date,
    postType: string,
    location: {
        latitude: string,
        longitude: string,
    },
    image: {
        imageFile: Buffer,
        description: string,
        extension: string
    }

}

export interface IPostDomainModel {
    username: string,
    message: string,
    postType: string,
    location: {
        latitude: string,
        longitude: string,
    },
    image: {
        imageFile: Buffer,
        description: string,
        extension: string
    }
}

let postsSchema = new mongoose.Schema({
    username: String,
    message: String,
    dateTime: {
        type: Date,
        default: Date.now,
    },
    postType: String,
    location: {
        latitude: String,
        longitude: String,
    },
    image: {
        imageFile: Buffer,
        description: String,
        extension: String
    }
});

export const PostModel: Model<IPost> = model<IPost>("Posts",postsSchema);